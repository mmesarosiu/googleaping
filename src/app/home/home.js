/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'ngBoilerplate.home', [
  'ui.router'    
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/home',
    views: {
      "main": {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    data:{ pageTitle: 'Home' }
  });
})

.controller( 'HomeCtrl', function ($scope) {

    $scope.currentUser = {};
    $scope.readyUser = false;
    
   
    
    $scope.login = function () {   
        gapi.auth.authorize({
            client_id: '779825117291-o56t8g98a0i834f8p4th9ul9k4aj30rm.apps.googleusercontent.com',
            scope: "https://www.googleapis.com/auth/plus.login",
            authuser: -1,            
            prompt: 'select_account'                      
        }, function(authResult) {
            if (authResult && !authResult.error) {
                $scope.currentUser = authResult;
                console.log('auth credentials ',authResult);                                
                gapi.client.load('plus', 'v1', function() {
                    gapi.client.plus.people.get({userId: 'me'}).execute(function (userData) {
                        angular.extend($scope.currentUser, userData);
                        $scope.$apply(function() {
                            $scope.displayUser = $scope.currentUser.displayName || $scope.currentUser.name.givenName || $scope.currentUser.emails[0].value;                             
                            $scope.readyUser = true;    
                        });
                        console.log('current user ', $scope.currentUser);                                                
                    });
                });                
            }            
        });                    
    };    

})

;

