Simple angular app for Google login. 
First, run npm install and bower install. Run grunt build.

In order to run the app, you will need the npm module http-server. 
Install it with: npm install -g http-server. 
More info about it: [https://www.npmjs.com/package/http-server](https://www.npmjs.com/package/http-server). 

Next steps:

**cd build** 

**http-server**

Be aware that you should use the 127.0.0.1:8080 serving url as this one has been set as Javascript origins for the clientId that we use for google login.

For any concerns, please contact mmesarosiu@macadamian.com.